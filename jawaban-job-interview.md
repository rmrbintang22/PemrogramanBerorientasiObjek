# no 1
Inisialisasi Game:

Membuat instance player dengan atribut-atribut awal, seperti health, attack power, dan gold.
Membuat instance level dengan levelNumber awal, hadiahGold, dan difficulty level.

Pertarungan:

Memulai pertarungan antara player dan enemy di level yang ditentukan.
Player dan enemy saling serang bergantian sampai salah satu pihak kalah.
Setiap serangan mengurangi health lawan sesuai dengan attack power.
Menentukan pemenang berdasarkan kondisi tertentu, misalnya health mencapai 0.

Leveling:

Jika player menang dalam pertarungan, naikkan levelNumber.
Update atribut-atribut player, seperti health dan attack power, berdasarkan levelNumber baru.
Tingkatkan kesulitan pertarungan di level yang lebih tinggi.

Shop:

Membuat instance shop dengan daftar senjata dan armor yang tersedia.
Menampilkan daftar senjata dan armor kepada player.
Memungkinkan player untuk membeli senjata atau armor dengan menggunakan gold.
Memperbarui atribut-atribut player berdasarkan senjata atau armor yang dibeli.

Penanganan Kekalahan:

Jika player kalah dalam pertarungan, kembalikan health dan atribut lainnya ke nilai awal.
Player dapat memilih untuk mencoba lagi atau keluar dari permainan.

Penanganan Kemenangan dan Permainan:

Jika player berhasil melewati semua level, tampilkan pesan kemenangan.
Player dapat memilih untuk bermain kembali atau keluar dari permainan.


Setiap langkah di atas akan diimplementasikan dengan algoritma pemrograman yang sesuai, termasuk penggunaan struktur data seperti array, list, dan objek untuk menyimpan dan mengelola data. Penerapan algoritma pemrograman yang efisien juga akan diperhatikan untuk memastikan permainan berjalan dengan lancar dan responsif.

untuk source code secara lengkap, saya lampirkan di nomor 2.

# no 2
import java.util.Scanner;
public class ShadowFightGame {
    public static void main(String[] args) {
        // Membuat objek player
        Scanner input = new Scanner(System.in);
        System.out.print(" Masukkan nama untuk karakter anda : ");
        String namaPlayer = input.nextLine();

        Player player = new Player(namaPlayer, 100, 10, 10000);

        //enemy
        Level level1 = new Level(1, 5000);
        Level level2 = new Level(2, 10000);

        //Shop
        Shop shop = new Shop();
        //main menu
        while (true) {
            player.tampilkanInformasi();
            player.tampilkanSenjata();
            player.tampilkanArmor();
            System.out.println("---- Main Menu ----");
            System.out.println("1. Story Mode ");
            System.out.println("2. Shop ");
            System.out.println("3. Exit ");
            System.out.println();
            System.out.println(" Masukkan pilihan : ");
            int pilihan = input.nextInt();

            if (pilihan == 1) {
                System.out.println("Mode Story");
                System.out.println("Selamat datang di Mode Story!");
                System.out.println("Mulai petualanganmu!");

                boolean isPlaying = true;
                int currentLevel = 1;

                while (isPlaying) {
                    System.out.println("\n=== Level " + currentLevel + " ===");

                    // Inisialisasi musuh berdasarkan level
                    Enemy enemy;
                    if (currentLevel == 1) {
                        enemy = level1.createEnemy();
                    } else if (currentLevel == 2) {
                        enemy = level2.createEnemy();
                    } else {
                        // Jika level berikutnya belum diimplementasikan, keluar dari loop
                        System.out.println("Tamat! Semua level telah selesai.");
                        break;
                    }

                    // Pertarungan
                    Pertarungan pertarungan = new Pertarungan(player, enemy);
                    pertarungan.mulai();

                    // Cek hasil pertarungan
                    if (pertarungan.isPlayerMenang()) {
                        int hadiahGold = level1.getHadiahGold();
                        player.tambahGold(hadiahGold);
                        System.out.println("Kamu mendapatkan hadiah gold sebesar " + hadiahGold);
                        currentLevel++;
                    } else {
                        isPlaying = false;
                    }

                }
            }
            else if (pilihan == 2) {
                    player.tampilkanInformasi();
                    shop.tampilkanShop(player);


                }else {
                System.exit(0);
            }
        }
    }
}

import java.util.ArrayList;
import java.util.List;

abstract class Entity {
    private String name;
    private int health;
    private int attackPower;
    private List<Senjata> senjataList;
    private List<Armor>armorList;



    // Constructor
    public Entity(String name, int health, int attackPower) {
        this.name = name;
        this.health = health;
        this.attackPower = attackPower;
        this.senjataList = new ArrayList<>();
        this.armorList = new ArrayList<>();
    }

    // Abstract method untuk melakukan serangan
    public abstract void attack(Entity target);

    // Getter dan setter untuk name, health, dan attackPower
    // (encapsulation)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }


    // Metode untuk mengambil damage
    public void takeDamage(int damage) {
        health -= damage;
        System.out.println(name + " takes " + damage + " damage.");
        if (health <= 0) {
            System.out.println(name + " is defeated.");
        }
    }

    public void tampilkanSenjata() {
        System.out.println("Senjata yang kamu gunakan : ");
        for (Senjata senjata : senjataList) {
            System.out.println("-" + senjata.getNama());
            System.out.println("\n");
        }
    }

    public void tampilkanArmor(){
        System.out.println("Armor yang kamu gunakan");
        for(Armor armor : armorList){
            System.out.println("-"+armor.getNama());
            System.out.println("\n");
        }
    }

    public void tambahSenjata(Senjata senjata){
        senjataList.add(senjata);
    }
    public void tambahArmor(Armor armor){
        armorList.add(armor);
    }

    public void equipSenjata(Senjata senjata){
        this.attackPower += senjata.getBonusAttackPower();
    }

    public void equipArmor(Armor armor){
        this.health += armor.getBonusHp();
    }
    private int totalHp(){
        int total = 0;
        for(Armor armor : armorList){
            total += armor.getBonusHp();
        }
        return total;
    }
    public void resetHP(){
        int defHP = 100;
        int totalHpBonus = totalHp();
        setHealth(defHP+totalHpBonus);
    }



}

public class Level {
    private int hadiahGold;
    private int levelNumber;

    public Level(int levelNumber, int getHadiahGold){
        this.levelNumber = levelNumber;
        this.hadiahGold = getHadiahGold;
    }
    public int getHadiahGold() {
        return hadiahGold;
    }

    public Enemy createEnemy() {
        // implementasi pembuatan musuh sesuai level
        if (levelNumber == 1) {
            return new Enemy("Lynx", 500, 20, "Assassin boss");
        } else if (levelNumber == 2) {
            return new Enemy("Hermit", 750, 40, "Demon Boss");
        }else if (levelNumber == 3) {
            return new Enemy("Butcher", 800, 60, "Bandit Boss");
        }else if (levelNumber == 4) {
            return new Enemy("Wasp", 890, 80, "Pirate Queen");
        }else if (levelNumber == 5) {
            return new Enemy("Widow", 923, 120, "Geisha");
        }else if (levelNumber == 6) {
            return new Enemy("Shogun", 1000, 180, "Warlord");
        }else if (levelNumber == 7) {
            return new Enemy("Hermit", 1500, 200, "Intergalactic Conqueror");
        } else {
            return null;
        }
    }
}

import java.util.ArrayList;
import java.util.Scanner;

public class Shop {

    private ArrayList<Senjata> daftarSenjata;
    private ArrayList<Armor>armors;
    Scanner input = new Scanner(System.in);

    public Shop() {
            daftarSenjata = new ArrayList<>();
            daftarSenjata.add(new Senjata("knives", 5000, 20));
            daftarSenjata.add(new Senjata("knuckles", 10000, 40));
            daftarSenjata.add(new Senjata("steel nunchaku", 16000, 80));
            daftarSenjata.add(new Senjata("Grim Scythe", 25000, 120));
            daftarSenjata.add(new Senjata("Devastator", 32000, 200));

            armors = new ArrayList<>();
            armors.add(new Armor("Monks Robe", 3000, 250 ));
            armors.add(new Armor("Sentinel's armor", 6000, 320 ));
            armors.add(new Armor("Evening Sunset", 10000, 400 ));
            armors.add(new Armor("Deathfall", 19000, 850 ));
            armors.add(new Armor("Heart of kshatrya", 25000, 1300 ));
    }

    public void tampilkanDaftarSenjata() {
        System.out.println("Daftar senjata di toko:");
        for (int i = 0; i < daftarSenjata.size(); i++) {
            Senjata senjata = daftarSenjata.get(i);
            System.out.println((i + 1) + ". " + senjata.getNama() + " (bonus attack power: " + senjata.getBonusAttackPower() + ", harga: " + senjata.getHarga() + ")");
        }
    }

    public void tampilkanDaftarArmor(){
        System.out.println("Daftar armor di toko :");
        for (int i = 0; i < armors.size(); i++) {
            Armor armor = armors.get(i);
            System.out.println((i + 1) + ". " + armor.getNama() + " (bonus attack power: " + armor.getBonusHp() + ", harga: " + armor.getHarga() + ")");
        }
    }

    public Senjata beliSenjata(Player player) {
        player.goldShop();
        tampilkanDaftarSenjata();
        System.out.println();
        System.out.print("Silahkan pilih senjata yang ingin anda beli : ");
        int indeks = input.nextInt();

        if (indeks >= 1 && indeks <= daftarSenjata.size()) {
            Senjata senjata = daftarSenjata.get(indeks - 1);
            if (player.getGold() >= senjata.getHarga()) {
                player.kurangiGold(senjata.getHarga());
                player.tambahSenjata(senjata);
                player.equipSenjata(senjata);
                return senjata;
            } else {
                System.out.println("Gold anda tidak cukup untuk membeli senjata " + senjata.getNama() + ".");
            }
        } else {
            System.out.println("Senjata dengan indeks " + indeks + " tidak ditemukan di toko.");
        }
        return null;
    }
    public Armor beliArmor(Player player) {
        player.goldShop();
        tampilkanDaftarArmor();
        System.out.println();
        System.out.print("Silahkan pilih armor yang ingin anda beli : ");
        int indeks = input.nextInt();

        if (indeks >= 1 && indeks <= armors.size()) {
            Armor armor = armors.get(indeks - 1);
            if (player.getGold() >= armor.getHarga()) {
                player.kurangiGold(armor.getHarga());
                player.tambahArmor(armor);
                player.equipArmor(armor);
                return armor;
            } else {
                System.out.println("Gold anda tidak cukup untuk membeli senjata " + armor.getNama() + ".");
            }
        } else {
            System.out.println("Senjata dengan indeks " + indeks + " tidak ditemukan di toko.");
        }
        return null;
    }

    public Shop tampilkanShop(Player player){
        System.out.println("Selamat Datang di Shop");
        System.out.println("=======================");
        System.out.println("1. beli Senjata");
        System.out.println("2. beli Armor");
        System.out.println("3. back ");
        System.out.print("Masukkan pilihan : ");
        int pil = input.nextInt();

        if(pil == 1){
            beliSenjata(player);
        } else if (pil == 2) {
            beliArmor(player);
        }
        else {

        }
        return null;
    }
}

public class Pertarungan {
        private Player player;
        private Enemy enemy;
        private boolean playerMenang;



        public Pertarungan(Player player, Enemy enemy) {
            this.player = player;
            this.enemy = enemy;
            this.playerMenang = false;
        }

        public boolean isPlayerMenang() {
            return playerMenang;
        }

        public void mulai() {
            System.out.println("Pertarungan dimulai!");
            enemy.tampilkanInformasi();

            while (player.getHealth() > 0 && enemy.getHealth() > 0) {
                player.attack(enemy);
                if (enemy.getHealth() <= 0) {
                    playerMenang = true;
                    break;
                }

                enemy.attack(player);
                if (player.getHealth() <= 0) {
                    break;
                }
            }

            if (playerMenang) {
                System.out.println("Anda menang dalam pertarungan!");
                player.resetHP();
            } else {
                System.out.println("Anda kalah dalam pertarungan!");
                player.resetHP();
            }
        }

        private void playerSerang() {
            int damage = player.getAttackPower();
            enemy.setHealth(enemy.getHealth() - damage);
            System.out.println("Player menyerang musuh dan mengurangi " + damage + " HP musuh.");
            enemy.tampilkanInformasi();
            System.out.println();
            System.out.println("========");
            System.out.println();
        }

        private void enemySerang() {
            int damage = enemy.getAttackPower();
            player.setHealth(player.getHealth() - damage);
            System.out.println("Musuh menyerang player dan mengurangi " + damage + " HP player.");
            player.tampilkanInformasi();
            System.out.println();
            System.out.println("========");
            System.out.println();
        }
    }


public class Senjata {
    private String nama;
    private int harga;
    private int bonusAttackPower;

    public Senjata(String nama, int harga, int bonusAttackPower) {
        this.nama = nama;
        this.harga = harga;
        this.bonusAttackPower = bonusAttackPower;
    }

    public String getNama() {
        return nama;
    }

    public int getHarga() {
        return harga;
    }

    public int getBonusAttackPower() {
        return bonusAttackPower;
    }
}

public class Armor {
    private String nama;
    private int harga;
    private int bonusHp;

    public Armor(String nama, int harga, int bonusHp){
        this.nama = nama;
        this.harga = harga;
        this.bonusHp = bonusHp ;
    }

    public String getNama(){
        return nama;
    }

    public int getHarga(){
        return harga;
    }

    public int getBonusHp(){
        return bonusHp;
    }

}

// Class untuk merepresentasikan karakter player dalam game
 class Player extends Entity {
    private int gold;
    private Armor armor;

    // Constructor
    public Player(String name, int health, int attackPower, int gold) {
        super(name, health, attackPower);
        this.gold = gold;
    }
    public int getGold(){
        return gold;
    }



    // Implementasi metode attack dari abstract class Entity
    @Override
    public void attack(Entity target) {
        System.out.println(getName() + " attacks " + target.getName() + " with " + getAttackPower() + " attack power.");
        target.takeDamage(getAttackPower());
    }

    //menampilkan informasi
    public void tampilkanInformasi() {
        System.out.println("=== Player ===");
        System.out.println("Nama: " + getName());
        System.out.println("HP : " + getHealth());
        System.out.println("Kekuatan Serangan: " + getAttackPower());
        System.out.println("gold : "+getGold());
    }

    public void goldShop(){
        System.out.println("Gold anda : "+getGold());
    }

    public void tambahGold(int jumlah){
        gold += jumlah;
    }

    public void kurangiGold(int jumlah){
        gold -= jumlah;
    }
}

// Class untuk merepresentasikan musuh dalam game
 class Enemy extends Entity {
    private String enemyType;
    // Constructor
    public Enemy(String name, int health, int attackPower, String enemyType) {
        super(name, health, attackPower);
        this.enemyType = enemyType;
    }

    public String getEnemyType() {
        return enemyType;
    }

    // Implementasi metode attack dari abstract class Entity
    @Override
    public void attack(Entity target) {
        System.out.println(getName() + " attacks " + target.getName() + " with " + getAttackPower() + " attack power.");
        target.takeDamage(getAttackPower());
    }
    public void tampilkanInformasi() {
        System.out.println("Enemy");
        System.out.println("Nama: " + getName());
        System.out.println("HP : " + getHealth());
        System.out.println("Kekuatan Serangan: " + getAttackPower());
        System.out.println("musuh tipe : "+getEnemyType());
    }

}


# no 3
https://www.youtube.com/watch?v=-qQmTavOfcI
# no 4
Untuk konsep encapsulation, saya menggunakannya hampir di semua class yang saya buat dalam game Shadow Fight, saya akan menampilkan beberapa contoh nya.

abstract class Entity {
    private String name;
    private int health;
    private int attackPower;
    private List<Senjata> senjataList;
    private List<Armor>armorList;
}

dalam class entity, dia mempunyai atribut seperti diatas dengan access modifier private. hal ini bertujuan untuk membatasi cara penggunaannya untuk membantu melindungi data dan mencegah akses langsung yang tidak diinginkan dari luar class.

adapun method seperti public String getName(); public int getHealth(); digunakan untuk mengakses dan memodifikasi nilai dari atribut atribut diatas.


# no 5
Saya menggunakan konsep abstraction dalam Abstract class Entity, berikut adalah Source code nya : 

import java.util.ArrayList;
import java.util.List;

abstract class Entity {
    private String name;
    private int health;
    private int attackPower;
    private List<Senjata> senjataList;
    private List<Armor>armorList;



    // Constructor
    public Entity(String name, int health, int attackPower) {
        this.name = name;
        this.health = health;
        this.attackPower = attackPower;
        this.senjataList = new ArrayList<>();
        this.armorList = new ArrayList<>();
    }

    // Abstract method untuk melakukan serangan
    public abstract void attack(Entity target);

    // Getter dan setter untuk name, health, dan attackPower
    // (encapsulation)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }


    // Metode untuk mengambil damage
    public void takeDamage(int damage) {
        health -= damage;
        System.out.println(name + " takes " + damage + " damage.");
        if (health <= 0) {
            System.out.println(name + " is defeated.");
        }
    }

    public void tampilkanSenjata() {
        System.out.println("Senjata yang kamu gunakan : ");
        for (Senjata senjata : senjataList) {
            System.out.println("-" + senjata.getNama());
            System.out.println("\n");
        }
    }

    public void tampilkanArmor(){
        System.out.println("Armor yang kamu gunakan");
        for(Armor armor : armorList){
            System.out.println("-"+armor.getNama());
            System.out.println("\n");
        }
    }

    public void tambahSenjata(Senjata senjata){
        senjataList.add(senjata);
    }
    public void tambahArmor(Armor armor){
        armorList.add(armor);
    }

    public void equipSenjata(Senjata senjata){
        this.attackPower += senjata.getBonusAttackPower();
    }

    public void equipArmor(Armor armor){
        this.health += armor.getBonusHp();
    }
    private int totalHp(){
        int total = 0;
        for(Armor armor : armorList){
            total += armor.getBonusHp();
        }
        return total;
    }
    public void resetHP(){
        int defHP = 100;
        int totalHpBonus = totalHp();
        setHealth(defHP+totalHpBonus);
    }
}

Abstract class entity berperan sebagai kerangka kerja umum untuk objek objek dalam permainan seperti player dan enemy. Melalui penggunaan abstraksi dalam abstract class Entity, program dapat memanfaatkan konsep inheritance dan polimorfisme untuk mengelompokkan objek-objek dengan karakteristik serupa, tetapi dengan implementasi yang berbeda sesuai dengan class turunannya.

Dengan menggunakan abstraksi, kita dapat mengelompokkan karakter-karakter dengan atribut-atribut dan perilaku yang sama ke dalam satu kelas abstract yang berfungsi sebagai kerangka kerja. Ini memungkinkan kita untuk mengelola karakter-karakter tersebut dengan cara yang konsisten dan mempermudah pengembangan permainan.

# no 6
Berikut adalah Source Code dari Implementasi Inheritance dan Polymorphysm

import java.util.ArrayList;
import java.util.List;

abstract class Entity {
    private String name;
    private int health;
    private int attackPower;
    private List<Senjata> senjataList;
    private List<Armor>armorList;



    // Constructor
    public Entity(String name, int health, int attackPower) {
        this.name = name;
        this.health = health;
        this.attackPower = attackPower;
        this.senjataList = new ArrayList<>();
        this.armorList = new ArrayList<>();
    }

    // Abstract method untuk melakukan serangan
    public abstract void attack(Entity target);

    // Getter dan setter untuk name, health, dan attackPower
    // (encapsulation)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }


    // Metode untuk mengambil damage
    public void takeDamage(int damage) {
        health -= damage;
        System.out.println(name + " takes " + damage + " damage.");
        if (health <= 0) {
            System.out.println(name + " is defeated.");
        }
    }

    public void tampilkanSenjata() {
        System.out.println("Senjata yang kamu gunakan : ");
        for (Senjata senjata : senjataList) {
            System.out.println("-" + senjata.getNama());
            System.out.println("\n");
        }
    }

    public void tampilkanArmor(){
        System.out.println("Armor yang kamu gunakan");
        for(Armor armor : armorList){
            System.out.println("-"+armor.getNama());
            System.out.println("\n");
        }
    }

    public void tambahSenjata(Senjata senjata){
        senjataList.add(senjata);
    }
    public void tambahArmor(Armor armor){
        armorList.add(armor);
    }

    public void equipSenjata(Senjata senjata){
        this.attackPower += senjata.getBonusAttackPower();
    }

    public void equipArmor(Armor armor){
        this.health += armor.getBonusHp();
    }
    private int totalHp(){
        int total = 0;
        for(Armor armor : armorList){
            total += armor.getBonusHp();
        }
        return total;
    }
    public void resetHP(){
        int defHP = 100;
        int totalHpBonus = totalHp();
        setHealth(defHP+totalHpBonus);
    }



}

// Class untuk merepresentasikan karakter player dalam game
 class Player extends Entity {
    private int gold;
    private Armor armor;

    // Constructor
    public Player(String name, int health, int attackPower, int gold) {
        super(name, health, attackPower);
        this.gold = gold;
    }
    public int getGold(){
        return gold;
    }



    // Implementasi metode attack dari abstract class Entity
    @Override
    public void attack(Entity target) {
        System.out.println(getName() + " attacks " + target.getName() + " with " + getAttackPower() + " attack power.");
        target.takeDamage(getAttackPower());
    }

    //menampilkan informasi
    public void tampilkanInformasi() {
        System.out.println("=== Player ===");
        System.out.println("Nama: " + getName());
        System.out.println("HP : " + getHealth());
        System.out.println("Kekuatan Serangan: " + getAttackPower());
        System.out.println("gold : "+getGold());
    }

    public void goldShop(){
        System.out.println("Gold anda : "+getGold());
    }

    public void tambahGold(int jumlah){
        gold += jumlah;
    }

    public void kurangiGold(int jumlah){
        gold -= jumlah;
    }
}
// Class untuk merepresentasikan musuh dalam game
 class Enemy extends Entity {
    private String enemyType;
    // Constructor
    public Enemy(String name, int health, int attackPower, String enemyType) {
        super(name, health, attackPower);
        this.enemyType = enemyType;
    }

    public String getEnemyType() {
        return enemyType;
    }

    // Implementasi metode attack dari abstract class Entity
    @Override
    public void attack(Entity target) {
        System.out.println(getName() + " attacks " + target.getName() + " with " + getAttackPower() + " attack power.");
        target.takeDamage(getAttackPower());
    }
    public void tampilkanInformasi() {
        System.out.println("Enemy");
        System.out.println("Nama: " + getName());
        System.out.println("HP : " + getHealth());
        System.out.println("Kekuatan Serangan: " + getAttackPower());
        System.out.println("musuh tipe : "+getEnemyType());
    }

}

Class player dan Enemy mewarisi sifat dan perilaku dari Abstract class Entity. kedua kelas tersebut menggunakan keyword 'Extend' untuk mewarisi Entity. sehingga dapat mengakses atribut dan method yang didefinisikan dalam Entity.

metode attack() dideklarasikan sebagai metode abstrak dalam kelas Entity. Kemudian, dalam kelas turunan (Player dan Enemy), metode attack() di-override menggunakan @Override untuk menandakan bahwa metode tersebut di-override dari kelas induk. ini adalah bentuk Implementasi dari Polymorphysm.


# no 7
Cara saya mendeskripsikan kumpulan use case ke dalam OOP adalah sebagai berikut.

1. mengidentifikasi Entitas yang terlibat, dalam Game shadow fight, terdapat class entity, player, enemy, senjata, armor, pertarungan, shop, level.

2. mengidentifikasi atribut dan method dalam entitas dan menentukannya. agar merepresentasikan perilaku atau tindakan yang dapat dilakukan oleh entitas tersebut.

3. membuat kelas untuk tiap entitasnya.

4. identifikasi hubungan antar kelas, seperti hubungan inheritance (pewarisan) antara kelas-kelas yang terkait atau hubungan asosiasi antara kelas-kelas yang memiliki keterkaitan.

5. Implementasi metode dan fungsionalitas untuk setiap kelas sesuai dengan perilaku dan aturan bisnis yang diinginkan. Gunakan konsep OOP seperti pewarisan, polimorfisme, dan enkapsulasi untuk membangun struktur yang baik.

6. Dalam implementasi proses bisnis, gunakan objek dari kelas-kelas yang telah dibuat untuk mewakili entitas-entitas yang terlibat. Interaksi antara objek-objek tersebut mencerminkan alur bisnis dalam game Shadow Fight.

7. Organisasikan kode dengan baik dengan membaginya ke dalam file-file yang sesuai.
# no 8
![](https://gitlab.com/rmrbintang22/PemrogramanBerorientasiObjek/-/raw/main/Untitled_Diagram.drawio__3_.png)

Use case table
| No | Use Case | Deskripsi | Nilai prioritas |
|--| ------ | ------ | ------                 |
| 1. | User memberikan nama pada player yang dibuat        | sebagai player, user dapat memberikan nama kepada playernya dengan bebas.       | 10                       |
| 2. |  Informasi Player      |   User dapat melihat informasi playernya, seperti HP, gold, attack power    |10|
| 3.| Fighting                       |player dapat melakukan pertarungan dengan Enemy|10|
|4.|Shop|user dapat membuka shop untuk membeli senjata yang dapat meningkatkan attack power |9|
|5.|Level|player dapat bertarung dengan Enemy yang berbeda di setiap levelnya, akan meningkat ketika player memenangkan pertarungan.|7|
|6.|Mengakhiri permainan|user dapat mengakhiri permainan kapanpun dia mau|10|
|7.|Achievement|player mendapatkan penghargaan jika menyelesaikan misi tertentu|5|
|8.|Menggunakan Skill khusus|Player atau enemy dapat menggunakan skill khusus|5|
|9.|Statistik Pertarungan|Akan menampilkan berapa kemenangan dan kekalahan yang telah dialami player|4|
|10.|Save Game|user dapat menyimpan data pencapaiannya|3|
|11.|Load Game|user dapat melanjutkan data dari permainan yang sebelumnya dia mainkan|3|
|12.|Heal|player dapat menggunakan potion untuk menambah darah yang bisa di beli di shop|2|
|13.|mode PvP|player dapat bertarung dengan player lain|2|
|14.|Upgrade senjata|user dapat meningkatkan kemampuan senjatanya|2|


# no 9
https://www.youtube.com/watch?v=hhHjti7t-hw
# no 10
![](https://gitlab.com/rmrbintang22/PemrogramanBerorientasiObjek/-/raw/main/Screenshot__558_.png)

![](https://gitlab.com/rmrbintang22/PemrogramanBerorientasiObjek/-/raw/main/Screenshot__559_.png)

![](https://gitlab.com/rmrbintang22/PemrogramanBerorientasiObjek/-/raw/main/Screenshot__560_.png)

![](https://gitlab.com/rmrbintang22/PemrogramanBerorientasiObjek/-/raw/main/Screenshot__561_.png)

![](https://gitlab.com/rmrbintang22/PemrogramanBerorientasiObjek/-/raw/main/Screenshot__562_.png)


